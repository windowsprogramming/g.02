// Simple_Macro.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "Simple_Macro.h"
#include <fstream>
#define MAX_LOADSTRING 100

//#define IMPORT __declspec(dllimport)
//#define EXPORT __declspec(dllexport)
//#define MY_EXPORT extern "C" __declspec(dllexport)
//#define MY_IMPORT extern "C" __declspec(dllimport)

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

DWORD start_time; //Starting time of recording

HHOOK hook_handle;
HHOOK getmsg_hook;



int repeat_count = 0;
int repeat_num = 1;

HWND hWnd;
HWND hPBButton;
HWND hRecordButton;
//Are we playing back
BOOL playing_back = FALSE;
//Are we recording
BOOL recording = FALSE;

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	DialogProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

LRESULT CALLBACK JournalRecordProc(int code, WPARAM wparam, LPARAM lparam);
LRESULT CALLBACK JournalPlaybackProc(int code, WPARAM wparam, LPARAM lparam);

void onRecord();
void onPlayBack();

FILE *My_Record_Log = NULL;
FILE *My_Record = NULL;
/////////////////////TRYADDED///////////////////
//Should we move to the next message (set when HC_SKIP is called)
static BOOL continue_move = TRUE;

//To keep track of messages sent so far, used to produce time interval
static int mesg_count = 0;

//Is there another message to playback?
static BOOL next_exist = TRUE;

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPTSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_SIMPLE_MACRO, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	hInst = hInstance; // Store instance handle in our global variable
	/*HWND hWnd;*/
	hWnd = CreateDialogParam(hInst, MAKEINTRESOURCE(IDD_MACRO), 0, (DLGPROC)DialogProc, 0);

	if (!hWnd)
	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_SIMPLE_MACRO));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (msg.message == WM_CANCELJOURNAL)
		{
			fclose(My_Record);
			fclose(My_Record_Log);
			UnhookWindowsHookEx(hook_handle);
			MessageBox(hWnd, L"Stop Recording!", L"ERROR", MB_OK);
		}
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg) && !IsDialogMessage(hWnd, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int)msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= DialogProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_SIMPLE_MACRO));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_SIMPLE_MACRO);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK DialogProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	hPBButton = GetDlgItem(hWnd, IDC_PLAYBACK);
	hRecordButton = GetDlgItem(hWnd, IDC_RECORD);
	/*if (message_list==NULL)
		EnableWindow(hPBButton, FALSE);*/
	switch (message)
	{
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDC_RECORD:
			onRecord();
			break;
		case IDC_PLAYBACK:
			onPlayBack();
			break;
		case IDC_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		case IDCANCEL:
			DestroyWindow(hWnd);
			return (INT_PTR)TRUE;
		}
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		return (INT_PTR)TRUE;
	}
	return (INT_PTR)FALSE;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

void onRecord()
{
	
	if (playing_back)
		return;

	start_time = GetTickCount();

	//EnableWindow(hPBButton, FALSE);
	EnableWindow(hRecordButton, FALSE);
	ShowWindow(hWnd, SW_MINIMIZE);

	My_Record_Log = fopen("c:\\Recording_Log.txt", "w");
	My_Record = fopen("c:\\Recording.txt", "wb");
	hook_handle = SetWindowsHookEx(WH_JOURNALRECORD, JournalRecordProc, hInst, 0);
	
	recording = !recording;
}

void onPlayBack()
{

	int num;
	BOOL GetInt;
	num = GetDlgItemInt(hWnd, IDC_EDIT_TIME, &GetInt, FALSE);
	if (GetInt == FALSE || num<1 || num>10)
	{
		MessageBox(hWnd, L"Valua is invalid or larger than 10!", L"ERROR", NULL);
		SetFocus(GetDlgItem(hWnd, IDC_EDIT_TIME));
		return;
	}
	repeat_num = num;
	//start_time = GetTickCount();

	playing_back = TRUE;
		
	ShowWindow(hWnd,SW_MINIMIZE);

	hook_handle = SetWindowsHookEx(WH_JOURNALPLAYBACK, JournalPlaybackProc, hInst, 0);

}
//static POSITION list_position;
/////////////////////////////////////////////TRYADDED///////////////////////////////////////////////
LRESULT CALLBACK JournalRecordProc(int code, WPARAM wparam, LPARAM lparam)
{

	EVENTMSG *mesg = NULL;


	if (code != HC_ACTION)
	{
		return CallNextHookEx(hook_handle, code, wparam, lparam);
	}
	mesg = (EVENTMSG *)lparam;
	
	fwrite(mesg, sizeof(EVENTMSG), 1, My_Record);
	fprintf(My_Record_Log, "HWND:%d, MSG:%d, WPARAM:%d, LPARAM:%d\n", mesg->hwnd, mesg->message, mesg->paramH, mesg->paramL);
	
	return CallNextHookEx(hook_handle, code, wparam, lparam);
}

LRESULT CALLBACK JournalPlaybackProc(int code, WPARAM wparam, LPARAM lparam)
{
	//Holds the struct read from list
	static EVENTMSG msg_load;

	//To compute how much we need to sleep
	static int prev_timeout = 0;
	static int cur_timeout = 0;
	int timeout = 0;
	static FILE *open = NULL;
	//Used to access the list sequentially when we are playing back from it.
	int read_in = 0;
	if (open == NULL)
	{
		open = fopen("c:\\Recording.txt", "rb");
		read_in = fread(&msg_load, sizeof(EVENTMSG), 1, open);
		prev_timeout = msg_load.time;
		cur_timeout = msg_load.time;
	}
	
	static int continue_playing = TRUE;

	switch (code)
	{
	case HC_SYSMODALON:
		continue_playing = FALSE;
		return 0;
	//if we turn off the dialog
	case HC_SYSMODALOFF:
		continue_playing = TRUE;
		return 0;

	case HC_SKIP:
		if (continue_playing == FALSE)
		{
			return 0;
		}

		read_in = fread(&msg_load, sizeof(EVENTMSG), 1, open);
		prev_timeout = cur_timeout;
		cur_timeout = msg_load.time;

		if (read_in!=1)
		{
			fclose(open);
			UnhookWindowsHookEx(hook_handle);
			MessageBox(NULL, L"Playingback complete", L"Announcement", MB_OK);
			ShowWindow(hWnd, SW_RESTORE);
		}
		return 0;
	}

	if (code != HC_GETNEXT || continue_playing == FALSE)
	{
		if (code < 0)
		{
			return CallNextHookEx(hook_handle, code, wparam, lparam);
		}
		return 0;
	}

	memcpy((void *)lparam, (void *)&msg_load, sizeof(msg_load));


	if (cur_timeout != prev_timeout)
	{
		timeout = (cur_timeout > prev_timeout ? (cur_timeout - prev_timeout) : \
			(cur_timeout + (0xffffffff - prev_timeout)));
		prev_timeout = cur_timeout;
		return timeout;
	}
	else
	{
		return 0;
	}
}
