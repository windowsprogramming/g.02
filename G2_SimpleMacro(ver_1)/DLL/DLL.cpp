// DLL.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include <afxtempl.h>

#define IMPORT __declspec(dllimport)
#define EXPORT __declspec(dllexport)
#define MY_EXPORT extern "C" __declspec(dllexport)
#define MY_IMPORT extern "C" __declspec(dllimport)
void uninstall_playback_hook();

extern HWND hWnd;
extern HWND hPBButton;
extern HWND hRecordButton;
//Are we playing back
extern BOOL playing_back;
//Are we recording
extern BOOL recording;

extern HHOOK hook_handle;

extern HHOOK getmsg_hook;

extern CList <EVENTMSG, EVENTMSG> *message_list;

extern DWORD start_time;

//Should we move to the next message (set when HC_SKIP is called)
static BOOL continue_move = TRUE;

//To keep track of messages sent so far, used to produce time interval
static int mesg_count = 0;

//How many times have we repeated playback
static int repeat_count = 0;
//Number of playback
extern int repeat_num;

//Is there another message to playback?
static BOOL next_exist = TRUE;


EXPORT LRESULT CALLBACK JournalRecordProc(int code, WPARAM wparam, LPARAM lparam)
{
	//The recording callback hook function

	SHORT KeyPause = GetKeyState(VK_PAUSE);
	SHORT KeyESC = GetKeyState(VK_ESCAPE);
	
	//Stop Process base on button click receive on GetMessage hook
	if (KeyPause & 0x80000000 || KeyESC & 0x80000000)
	{
		//StopRecording();
		UnhookWindowsHookEx(hook_handle);
		EnableWindow(hPBButton, TRUE);
		EnableWindow(hRecordButton, TRUE);
		recording = !recording;
		return CallNextHookEx(hook_handle, code, wparam, lparam);
	}

	EVENTMSG *mesg = NULL;
	static EVENTMSG msg_save;

	switch (code)
	{
	case HC_ACTION:

		mesg = (EVENTMSG *)lparam;

		if (mesg->hwnd != hWnd)
		{
			
			mesg->time = mesg->time - start_time;

			msg_save.hwnd = mesg->hwnd;
			msg_save.message = mesg->message;
			msg_save.paramH = mesg->paramH;
			msg_save.paramL = mesg->paramL;
			msg_save.time = mesg->time;

			//Save the action into message_list
			message_list->AddTail(msg_save);
			
		}
		break;

	default:
		return CallNextHookEx(hook_handle, code, wparam, lparam);

	}

	return 0;
}

EXPORT LRESULT CALLBACK JournalPlaybackProc(int code, WPARAM wparam, LPARAM lparam)
{
	EVENTMSG *mesg;

	//Holds the struct read from file
	static EVENTMSG msg_save;

	//To compute how much we need to sleep
	LRESULT delta;

	//Used to access the list sequentially when we are playing back from it.
	static POSITION list_position;

	//The playback callback hook function

	switch (code)
	{
	case HC_GETNEXT:

		mesg = (EVENTMSG *)lparam;

		if (continue_move)
		{

			if (next_exist)
			{
				//Read the message saved in the list
				if (list_position != NULL)
				{
					msg_save = message_list->GetNext(list_position);
				}
				else
				{
					next_exist = FALSE;
				}

			}
			else
			{
				if (repeat_count >= repeat_num)
				{
					uninstall_playback_hook();
					MessageBox(hWnd, L"Playback Over", L"Simple_Macro V1.0", MB_OK);
					return 0;
				}
				else
				{
					repeat_count++;
					start_time = GetTickCount();

					//Reset everything else
					mesg_count = 0;
					continue_move = TRUE;
					next_exist = TRUE;

					//Initialize list position to start of list
					list_position = message_list->GetHeadPosition();

					//Clear Keyboard state (remove control,alt or other key presses)
					BYTE keyboard_state[256];
					GetKeyboardState(keyboard_state);
					keyboard_state[VK_CONTROL] = keyboard_state[VK_CONTROL] & 0x00000000;
					keyboard_state[VK_MENU] = keyboard_state[VK_MENU] & 0x00000000;

					SetKeyboardState(keyboard_state);

					//Flag to know that it is playing back
					playing_back = TRUE;

				}
				return 0;
			}
			continue_move = FALSE;
		}
		//Copy mesg from file to the lparam structure
		mesg->hwnd = msg_save.hwnd;
		mesg->message = msg_save.message;
		mesg->paramH = msg_save.paramH;
		mesg->paramL = msg_save.paramL;


		mesg->time = start_time + msg_save.time;

		//This measures the difference between when mesg is to played and current time.
		delta = mesg->time - GetTickCount();

		if (delta>0)
		{

			return delta;
		}
		else
		{
			return 0;
		}
		break;
	case HC_SKIP:
		//Move to the next message
		continue_move = TRUE;
		mesg_count++;
		break;
	default:
		return CallNextHookEx(hook_handle, code, wparam, lparam);
	}
	return 0;
}


EXPORT LRESULT CALLBACK GetMsgProc(int code, WPARAM wparam, LPARAM lparam)
{
	//Respond to Ctrl+Break or Ctrl+Esc or Ctrl+Alt+Del (StopRecording)

	if (code <0)
	{
		return CallNextHookEx(getmsg_hook, code, wparam, lparam);
	}
	if (code == HC_ACTION)
	{
		MSG *msg = (MSG *)lparam;

		if (msg->message == WM_CANCELJOURNAL)
		{
			//If user interrupted playback. 
			if (playing_back)
			{
				uninstall_playback_hook();

				MessageBox(hWnd, L"Playback Stop", L"Simple_Macro V1.0", MB_OK);

			}
			else if (recording)
			{
				//StopRecording();
				UnhookWindowsHookEx(hook_handle);
				EnableWindow(hPBButton, TRUE);
				EnableWindow(hRecordButton, TRUE);
				recording = !recording;

			}
		}
	}
	else
	{
		return CallNextHookEx(getmsg_hook, code, wparam, lparam);
	}
	return 0;
}


void uninstall_playback_hook()
{
	UnhookWindowsHookEx(hook_handle);
	playing_back = FALSE;
	repeat_count = 0;

	//Call this so that app is restored from taskbar
	ShowWindow(hWnd,SW_RESTORE);

	mesg_count = 0;
	continue_move = TRUE;
	next_exist = TRUE;
}
