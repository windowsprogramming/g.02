//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Simple_Macro.rc
//
#define IDC_MYICON                      2
#define IDD_SIMPLE_MACRO_DIALOG         102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_SIMPLE_MACRO                107
#define IDI_SMALL                       108
#define IDC_SIMPLE_MACRO                109
#define IDR_MAINFRAME                   128
#define IDD_MACRO                       129
#define IDC_RECORD                      1000
#define IDC_PLAYBACK                    1001
#define IDC_ABOUT                       1002
#define IDC_EDIT1                       1003
#define IDC_EDIT_TIME                   1003
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1004
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
