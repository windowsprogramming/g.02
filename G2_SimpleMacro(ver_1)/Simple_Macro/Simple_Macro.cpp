// Simple_Macro.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "Simple_Macro.h"
#include <afxtempl.h>
#define MAX_LOADSTRING 100

//#define IMPORT __declspec(dllimport)
//#define EXPORT __declspec(dllexport)
//#define MY_EXPORT extern "C" __declspec(dllexport)
//#define MY_IMPORT extern "C" __declspec(dllimport)

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

DWORD start_time; //Starting time of recording

HHOOK hook_handle;
HHOOK getmsg_hook;

CList <EVENTMSG, EVENTMSG> *message_list;

int repeat_count = 0;
int repeat_num = 1;

HWND hWnd;
HWND hPBButton;
HWND hRecordButton;
//Are we playing back
BOOL playing_back = FALSE;
//Are we recording
BOOL recording = FALSE;

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	DialogProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

LRESULT CALLBACK JournalRecordProc(int code, WPARAM wparam, LPARAM lparam);
LRESULT CALLBACK JournalPlaybackProc(int code, WPARAM wparam, LPARAM lparam);
LRESULT CALLBACK GetMsgProc(int code, WPARAM wparam, LPARAM lparam);
void uninstall_playback_hook();
void onRecord();
void onPlayBack();

/////////////////////TRYADDED///////////////////
//Should we move to the next message (set when HC_SKIP is called)
static BOOL continue_move = TRUE;

//To keep track of messages sent so far, used to produce time interval
static int mesg_count = 0;

//Is there another message to playback?
static BOOL next_exist = TRUE;

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPTSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_SIMPLE_MACRO, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	hInst = hInstance; // Store instance handle in our global variable
	/*HWND hWnd;*/
	hWnd = CreateDialogParam(hInst, MAKEINTRESOURCE(IDD_MACRO), 0, (DLGPROC)DialogProc, 0);
	getmsg_hook = SetWindowsHookEx(WH_GETMESSAGE, GetMsgProc, hInst, 0);

	if (!hWnd)
	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_SIMPLE_MACRO));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg) && !IsDialogMessage(hWnd, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int)msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= DialogProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_SIMPLE_MACRO));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_SIMPLE_MACRO);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK DialogProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	hPBButton = GetDlgItem(hWnd, IDC_PLAYBACK);
	hRecordButton = GetDlgItem(hWnd, IDC_RECORD);
	/*if (message_list==NULL)
		EnableWindow(hPBButton, FALSE);*/
	switch (message)
	{
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDC_RECORD:
			onRecord();
			break;
		case IDC_PLAYBACK:
			onPlayBack();
			break;
		case IDC_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			UnhookWindowsHookEx(getmsg_hook);
			DestroyWindow(hWnd);
			break;
		case IDCANCEL:
			UnhookWindowsHookEx(getmsg_hook);
			DestroyWindow(hWnd);
			return (INT_PTR)TRUE;
		}
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		return (INT_PTR)TRUE;
	}
	
	return (INT_PTR)FALSE;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

void onRecord()
{
	if (message_list != NULL)
		delete message_list;
	message_list = new CList <EVENTMSG, EVENTMSG>();
	MessageBox(hWnd, L"Create List", L"Announce", MB_OK);
	
	if (playing_back)
		return;

	start_time = GetTickCount();

	//EnableWindow(hPBButton, FALSE);
	EnableWindow(hRecordButton, FALSE);
	ShowWindow(hWnd, SW_MINIMIZE);
	hook_handle = SetWindowsHookEx(WH_JOURNALRECORD, JournalRecordProc, hInst, 0);
	
	recording = !recording;
}

void onPlayBack()
{
	if (message_list == NULL)
	{
		MessageBox(hWnd, L"Playback Fail! Nothing recorded!", L"Error", MB_OK);
		return;
	}
	int num;
	BOOL GetInt;
	num = GetDlgItemInt(hWnd, IDC_EDIT_TIME, &GetInt, FALSE);
	if (GetInt == FALSE || num<1 || num>10)
	{
		MessageBox(hWnd, L"Valua is invalid or larger than 10!", L"ERROR", NULL);
		SetFocus(GetDlgItem(hWnd, IDC_EDIT_TIME));
		return;
	}
	repeat_num = num;
	start_time = GetTickCount();

	playing_back = TRUE;
		
	ShowWindow(hWnd,SW_MINIMIZE);

	hook_handle = SetWindowsHookEx(WH_JOURNALPLAYBACK, JournalPlaybackProc, hInst, 0);

}

/////////////////////////////////////////////TRYADDED///////////////////////////////////////////////
LRESULT CALLBACK JournalRecordProc(int code, WPARAM wparam, LPARAM lparam)
{
	MessageBox(hWnd, L"Stop Recording!", L"ERROR", MB_OK);
	//The recording callback hook function

	SHORT KeyPause = GetKeyState(VK_PAUSE);
	SHORT KeyESC = GetKeyState(VK_ESCAPE);

	//Stop Process base on button click receive on GetMessage hook
	if (KeyPause & 0x80000000 || KeyESC & 0x80000000)
	{
		//StopRecording();
		UnhookWindowsHookEx(hook_handle);
		EnableWindow(hPBButton, TRUE);
		EnableWindow(hRecordButton, TRUE);
		recording = !recording;
		MessageBox(hWnd, L"Stop Recording!", L"ERROR", MB_OK);
		return CallNextHookEx(hook_handle, code, wparam, lparam);
	}

	EVENTMSG *mesg = NULL;
	static EVENTMSG msg_save;

	switch (code)
	{
	case HC_ACTION:

		mesg = (EVENTMSG *)lparam;

		if (mesg->hwnd != hWnd)
		{

			mesg->time = mesg->time - start_time;

			msg_save.hwnd = mesg->hwnd;
			msg_save.message = mesg->message;
			msg_save.paramH = mesg->paramH;
			msg_save.paramL = mesg->paramL;
			msg_save.time = mesg->time;

			//Save the action into message_list
			message_list->AddTail(msg_save);

		}
		break;

	default:
		return CallNextHookEx(hook_handle, code, wparam, lparam);

	}

	return 0;
}

LRESULT CALLBACK JournalPlaybackProc(int code, WPARAM wparam, LPARAM lparam)
{
	EVENTMSG *mesg;

	//Holds the struct read from file
	static EVENTMSG msg_load;

	//To compute how much we need to sleep
	LRESULT delta;

	//Used to access the list sequentially when we are playing back from it.
	static POSITION list_position;
	list_position = message_list->GetHeadPosition();
	//The playback callback hook function

	switch (code)
	{
	case HC_GETNEXT:

		mesg = (EVENTMSG *)lparam;

		if (continue_move)
		{

			if (next_exist)
			{
				//Read the message saved in the list
				if (list_position != NULL)
				{
					msg_load = message_list->GetNext(list_position);
				}
				else
				{
					next_exist = FALSE;
				}

			}
			else
			{
				if (repeat_count >= repeat_num)
				{
					uninstall_playback_hook();
					MessageBox(hWnd, L"Playback Over", L"Simple_Macro V1.0", MB_OK);
					return 0;
				}
				else
				{
					repeat_count++;
					start_time = GetTickCount();

					//Reset everything else
					mesg_count = 0;
					continue_move = TRUE;
					next_exist = TRUE;

					//Initialize list position to start of list
					list_position = message_list->GetHeadPosition();

					//Clear Keyboard state (remove control,alt or other key presses)
					BYTE keyboard_state[256];
					GetKeyboardState(keyboard_state);
					keyboard_state[VK_CONTROL] = keyboard_state[VK_CONTROL] & 0x00000000;
					keyboard_state[VK_MENU] = keyboard_state[VK_MENU] & 0x00000000;

					SetKeyboardState(keyboard_state);

					//Flag to know that it is playing back
					playing_back = TRUE;

				}
				return 0;
			}
			continue_move = FALSE;
		}
		//Copy mesg from file to the lparam structure
		mesg->hwnd = msg_load.hwnd;
		mesg->message = msg_load.message;
		mesg->paramH = msg_load.paramH;
		mesg->paramL = msg_load.paramL;


		mesg->time = start_time + msg_load.time;

		//This measures the difference between when mesg is to played and current time.
		delta = mesg->time - GetTickCount();

		if (delta>0)
		{

			return delta;
		}
		else
		{
			return 0;
		}
		break;
	case HC_SKIP:
		//Move to the next message
		continue_move = TRUE;
		mesg_count++;
		break;
	default:
		return CallNextHookEx(hook_handle, code, wparam, lparam);
	}
	return 0;
}


LRESULT CALLBACK GetMsgProc(int code, WPARAM wparam, LPARAM lparam)
{
	//Respond to Ctrl+Break or Ctrl+Esc or Ctrl+Alt+Del (StopRecording)

	if (code <0)
	{
		return CallNextHookEx(getmsg_hook, code, wparam, lparam);
	}
	if (code == HC_ACTION)
	{
		MSG *msg = (MSG *)lparam;

		if (msg->message == WM_CANCELJOURNAL)
		{
			//If user interrupted playback. 
			if (playing_back)
			{
				uninstall_playback_hook();

				MessageBox(hWnd, L"Playback Stop", L"Simple_Macro V1.0", MB_OK);

			}
			else if (recording)
			{
				//StopRecording();
				UnhookWindowsHookEx(hook_handle);
				EnableWindow(hPBButton, TRUE);
				EnableWindow(hRecordButton, TRUE);
				recording = !recording;

			}
		}
	}
	else
	{
		return CallNextHookEx(getmsg_hook, code, wparam, lparam);
	}
	return 0;
}


void uninstall_playback_hook()
{
	UnhookWindowsHookEx(hook_handle);
	playing_back = FALSE;
	repeat_count = 0;

	//Call this so that app is restored from taskbar
	ShowWindow(hWnd, SW_RESTORE);

	mesg_count = 0;
	continue_move = TRUE;
	next_exist = TRUE;
}
